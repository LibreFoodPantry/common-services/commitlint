# Commitlint



## Getting Started
1. If you have .gitlab-ci.yml add the lines mentioned on step 3 to it
2. If not, create a file .gitlab.ci.yml and add the lines mentioned on step 3
3. add lines 6, and 11 through 34.

### What it does
- It will check your commit to see if they follow conventional commit rules 
> Follow this link to see what they look like www.conventionalcommits.org 
- If you push to the main branch and have a bad commit message it will fail the pipeline but if you push to any
branch other than main it won't fail the pipeline but it will give you a warning.

1. Create a file commitconfig.sh
2. Copy the command bellow and paste it in there
> echo "module.exports = {extends: ['@commitlint/config-conventional']}" > commitlint.config.js
### complete this in every repository in your project
